#!/usr/bin/env fish

echo '--- starting postgres docker container'
docker run -d --name postgresql-dev \
        -e POSTGRES_PASSWORD='en to tre fire' \
        --mount source=postgresql-dev,target=/var/lib/postgresql/data \
        -p 5432:5432 \
        postgres

# vim: set filetype=fish:
