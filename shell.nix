{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "dotnet-dev";
  buildInputs = [
    pkgs.dotnet-sdk_5
  ];

  shellHook = ''
    echo "Starting dotnet dev..."
  '';

  DOTNET_ROOT = pkgs.dotnet-sdk_5;
}
