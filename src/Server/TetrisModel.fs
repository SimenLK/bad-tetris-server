module TetrisModel

open System
open System.ComponentModel.DataAnnotations
open Microsoft.EntityFrameworkCore
open EntityFrameworkCore.FSharp.Extensions

[<CLIMutable>]
type User = {
    [<Key>] Id: int
    Username: string
    Created: DateOnly
    LastPlayed: DateOnly
}

[<CLIMutable>]
type Highscore = {
    [<Key>] Id: int
    UserId: int
    Score: int
    Played: DateOnly
}

type TetrisContext () =
    inherit DbContext ()
    
    [<DefaultValue>]
    val mutable users : DbSet<User>
    member this.Users with get() = this.users and set v = this.users <- v

    [<DefaultValue>]
    val mutable highscores : DbSet<Highscore>
    member this.Highscores with get() = this.highscores and set v = this.highscores <- v

    override _.OnModelCreating builder =
        builder.RegisterOptionTypes() // enables option values for all entities

    override __.OnConfiguring(options: DbContextOptionsBuilder) : unit =
        options.UseNpgsql(@"host=omikron;database=tetris;user id=postgres;") |> ignore
