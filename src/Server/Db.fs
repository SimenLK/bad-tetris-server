module Db

open System
open Microsoft.EntityFrameworkCore
open System.Linq

type Highscore = {
    UserId: int
    Score: int
    Played: DateTime
}

type User = {
    Id: int
    Username: string
    Created: DateTime
    LastPlayed: DateTime
}

type UserHighscore = {
    Name: string
    Score: int
}

let getContext () =
    let ctx = new TetrisModel.TetrisContext()
    ctx

let runQuery qry =
    try
        let ctx = getContext ()
        qry ctx |> Ok
    with
    | e ->
        printfn $"Db.runQuery: Exception: {e.Message}\n{e.InnerException}"
        Error (string e)

let tryMigrate () = runQuery (fun ctx -> ctx.Database.Migrate())

let toHighscore (h: TetrisModel.Highscore) : Highscore =
    let played : DateTime = DateTime.Parse(h.Played.ToString())
    { UserId = h.UserId
      Score = h.Score
      Played = played }

let toUserHighscore ((h, u): Highscore * User) =
    { Name = u.Username
      Score = h.Score }

let fromHighscore (data: Highscore) : TetrisModel.Highscore =
    let date : DateOnly = DateOnly.FromDateTime data.Played
    { Id = 0
      UserId = data.UserId
      Score = data.Score
      Played = date }

let getUserId username =
    task {
        let qry (ctx: TetrisModel.TetrisContext) =
            ctx.Users.AsNoTracking()
                .Where(fun u -> u.Username = username)
                .Select(fun u -> u.Id)
                .Single()

        return runQuery qry
    }

let addHighscore (data: UserHighscore) =
    task {
        // TODO(SimenLK): Create user on no match
        match! getUserId data.Name with
        | Ok userId ->
            let newHighscore : Highscore =
                { UserId = userId
                  Score = data.Score
                  Played = DateTime.Now }
            let entry = fromHighscore newHighscore
            let qry (ctx : TetrisModel.TetrisContext) =
                ctx.Add entry |> ignore
                ctx.SaveChanges () |> ignore
                entry.Id
            return runQuery qry
        | Error e ->
            return Error e
    }

let getLeaderboard () =
    let qry (ctx : TetrisModel.TetrisContext) =
        ctx.Highscores.AsNoTracking()
            .OrderByDescending(fun x -> x.Score)
            .Join(ctx.Users,
                  (fun h -> h.UserId),
                  (fun u -> u.Id),
                  (fun h u -> { Name = u.Username; Score = h.Score }))
            .Take(10)

    runQuery qry
    |> Result.map (Seq.toList)
