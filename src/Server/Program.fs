﻿module Program

open System
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Giraffe


let handleAddHighscore next (ctx: HttpContext) =
    task {
        let! data = ctx.BindJsonAsync<Db.UserHighscore> ()
        match! Db.addHighscore data with
        | Ok _ ->
            // TODO(SimenLK): return the leaderboards
            return! Successful.created (text "Highscore added") next ctx
        | Error e ->
            return! RequestErrors.BAD_REQUEST (text e) next ctx
    }

let handleGetLeaderboards next ctx =
    task {
        match Db.getLeaderboard () with
        | Ok leaderboards ->
            return! json leaderboards next ctx
        | Error e ->
            return! RequestErrors.BAD_REQUEST (text e) next ctx
    }

let webapp =
    choose [
        GET >=> choose [
            route "/api" >=> text "hello bad tetris api!"
            route "/api/v1/leaderboards"
                >=> setHttpHeader "Access-Control-Allow-Origin" "*"
                >=> handleGetLeaderboards
        ]
        route "/api/v1/leaderboards"
            >=> setHttpHeader "Access-Control-Allow-Origin" "*"
            >=> choose [
                POST >=> handleAddHighscore
                // PUT
                // DELETE
            ]
    ]

let errorHandler (ex: Exception) (logger: ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse
    >=> ServerErrors.INTERNAL_ERROR ex.Message

let configureApp (app: IApplicationBuilder) =
    app.UseGiraffeErrorHandler(errorHandler)
        .UseGiraffe webapp

let configureServices (services: IServiceCollection) =
    services.AddGiraffe() |> ignore

let runWebApp () =
    Host.CreateDefaultBuilder()
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder
                    .Configure(configureApp)
                    .UseUrls("http://localhost:8085")
                    .ConfigureServices(configureServices)
                    |> ignore)
        .Build()
        .Run()

[<EntryPoint>]
let main _ =
    match Db.tryMigrate () with
    | Ok _ ->
        printfn "Done migrating to tetris db"
        runWebApp ()
        0
    | Error e ->
        printfn $"Exception in Db.tryMigrate:\n{e}"
        1
