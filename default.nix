{ pkgs ? import <nixpkgs> {}
}:
pkgs.buildDotnetModule {
  pname = "bad-tetris-server";
  version = "0.0.0";

  src = ./.;

  projectFile = "bad-tetris-server.fsproj";
  nugetDeps = ./deps.nix;
}
