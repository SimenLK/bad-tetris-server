{ pkgs ? import <nixpkgs> { system = "x86_64-linux"; }
}:
let
  bad-tetris-server = import ./default.nix { };
in
pkgs.dockerTools.buildLayeredImage {
  name = "bad-tetris-server";
  tag = "latest";
  contents = [ bad-tetris-server ];
}
